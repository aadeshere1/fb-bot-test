class CallbackController < ApplicationController
  protect_from_forgery with: :null_session
  def index
    if params["hub.verify_token"] == "dashain"
      @challenge = params["hub.challenge"]
    end
  end

  def received_data
    therequest = request.body.read
    data = JSON.parse(therequest)
    entries = data["entry"]
    entries.each do |entry|
      entry["messaging"].each do |message|
        sender = message["sender"]["id"]
        message = message["message"]["text"]
        return_json = { "recipient": {"id": "#{sender}"},"message": {"text": "#{message}"}}
        HTTP.post(url, json: return_json)
      end
    end
    render "received_data"
  end

  def url
    "https://graph.facebook.com/v2.6/me/messages?access_token=EAAECVrhGffQBAI7ZAYocWVQ1wzTbZC7EKgEQ8TOKCdXmi0ncn4X6wjTAIGsNJkXw7qEqQ9aWa6We3lJq4MWvfbySOo4MeZAO1lBGcFSLJ1PGUiwk5Hq0sL84EKZAUkuLe9AueIQrU1GawVg3Ab5qyLuWk7vtPfDpGEAvZBa0joPUy1NNE9dPP"
  end
end
